import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  Future<UserCredential> signIn(String email, String password);

  Future<UserCredential> signUp(String email, String password);

  //Future<UserCredential> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<bool> isEmailVerified(UserCredential user);

  Future<void> changeEmail(String email);

  Future<void> changePassword(String password);

  Future<void> deleteUser();

  Future<void> sendPasswordResetMail(String email);
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<UserCredential> signIn(String email, String password) async {
    UserCredential _userCredential;
    FirebaseAuthException authError;
    try {
      _userCredential = await _firebaseAuth
        .signInWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      if(e.runtimeType == FirebaseAuthException){
        authError = e;
        throw authError.message;
      } else {
        throw e;
      }
    }
    return _userCredential;
  }

  Future<UserCredential> signUp(String email, String password) async {
    FirebaseAuthException authError;
    UserCredential _userCredential;
    try {
       _userCredential = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((registeredUserCreds) {
              registeredUserCreds.user.sendEmailVerification();
              return registeredUserCreds;
              });
    } catch (e) {
      if(e.runtimeType == FirebaseAuthException){
        authError = e;
        throw authError.message;
      } else {
        throw e;
      }
    }
    return _userCredential;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<bool> isEmailVerified(UserCredential user) async {
    return user.user.emailVerified;
  }

  Future<void> sendEmailVerification() async {
    User user = _firebaseAuth.currentUser;
    user.sendEmailVerification();
  }

  @override
  Future<void> changeEmail(String email) async {
    User user = _firebaseAuth.currentUser;
    user.updateEmail(email).then((_) {
      print("Succesfull changed email");
    }).catchError((error) {
      print("email can't be changed" + error.toString());
    });
    return null;
  }

  @override
  Future<void> changePassword(String password) async {
    User user = _firebaseAuth.currentUser;
    user.updatePassword(password).then((_) {
      print("Succesfull changed password");
    }).catchError((error) {
      print("Password can't be changed" + error.toString());
    });
    return null;
  }

  @override
  Future<void> deleteUser() async {
    User user = _firebaseAuth.currentUser;
    user.delete().then((_) {
      print("Succesfull user deleted");
    }).catchError((error) {
      print("user can't be delete" + error.toString());
    });
    return null;
  }

  @override
  Future<void> sendPasswordResetMail(String email) async {
    await _firebaseAuth.sendPasswordResetEmail(email: email);
    return null;
  }
}
