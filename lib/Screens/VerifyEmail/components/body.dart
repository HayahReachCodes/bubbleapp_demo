import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/VerifyEmail/components/background.dart';
import 'package:flutter_auth/Screens/Welcome/welcome_screen.dart';
import 'package:flutter_auth/components/rounded_button.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/services/auth.dart';
import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Background(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Verify Your Email",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: size.height * 0.03),
                SvgPicture.asset(
                  "assets/icons/login.svg",
                  height: size.height * 0.35,
                ),
                SizedBox(height: size.height * 0.03),
                RoundedButton(
                  text: "Resend Verification Email",
                  press: () {
                    Auth().sendEmailVerification();
                  },
                ),
                SizedBox(height: size.height * 0.03)
              ],
            ),
          ),
        ),
        Positioned(
          top: 30,
          left: 15,
          child: IconButton(
            color: kPrimaryColor,
              icon: Icon(Icons.cancel_outlined, size: 30,),
              onPressed: () =>
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return WelcomeScreen();
                  }))),
        )
      ],
    );
  }
}
